Connexion Corporate Communications presents the Automated Web Translation Workflow (AWTW) 
module which allows you to export and import from the Drupal administration all � or part 
of � your website content.  With the AWTW module translating your web content has now 
become child�s play.

The content is exported into an XML format that is compatible with most Computer Assisted 
Translation tools (such as D�j�Vu, SDL/Trados, Wordfast, etc), allowing your human translators 
to use their own specialized work environment. After translation, you can import the translated 
content via the Drupal interface, with just a few mouse clicks. The module automatically creates 
your new language pages in the source layout and links them to the original items.

If you�re interested in this module, make sure to check out the demonstration video at: http://www.connexion.eu/awtw/

About us:
Connexion Corporate Communications is a Belgian business-to-business communications and language 
agency, and WCMS integrator.  Connexion has been operating on the international market since 1987. 
The agency was appointed Hot Banana Channel Partner for Europe in the Benelux and has gained Drupal 
expertise since several years. Clients calling upon Connexion�s online and offline communication 
expertise include Agoria, Ansell Healthcare, Atos Worldline, Belgacom, the Belgian government, 
Cognos, Electrabel, Ingersoll Rand, Isabel and Tractebel.  http://www.connexion.eu
