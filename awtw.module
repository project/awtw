<?php


/**
* Valid permissions for this module
* @return array An array of valid permissions for the onthisdate module
*/
function awtw_perm() {
  return array('access awtw');
} // function newmodule_perm

/**
* Implementation of the _menu hook
* In Administer > Content management there will be an AWTW link
* This page contains 3 tabs: Export, Import & Translations
*/
function awtw_menu() {
  $items = array();  

    $items[] = array(
	  'path' => 'admin/content/awtw', 
	  'title' => t('AWTW'),
      'access' => user_access('access awtw'),
      'callback' => 'awtw_admin',
      'description' => t('The Automated Web Translation Workflow module.')
	);
    $items[] = array(
	  'path' => 'admin/content/awtw/export', 
	  'title' => t('Export'),
	  'access' => user_access('access awtw'),
	  'type' => MENU_DEFAULT_LOCAL_TASK, 
	  'weight' => -10
	);
    $items[] = array(
	  'path' => 'admin/content/awtw/import', 
	  'title' => t('Import'),
	  'access' => user_access('access awtw'),
      'callback' => 'awtw_admin_import',
      'type' => MENU_LOCAL_TASK
	);
	$items[] = array(
	  'path' => 'admin/content/awtw/translation',
      'title' => t('Translations'),
      'description' => t("Manage content translations."),
      'callback' => 'translation_admin_content',
      'type' => MENU_LOCAL_TASK,
      'access' => user_access('access awtw')
	);
  return $items;
}

/**
 * Return the Export tab 
 */
function awtw_admin() {
  $output = "";
  $filter_form = drupal_get_form('awtw_filter_form');
  $selectnodes_form = drupal_get_form('awtw_selectnodes_form');
  $output = $filter_form . $selectnodes_form;
  return $output;
}

/**
 * Return the import tab
 */
function awtw_admin_import() {
  $output = "";
  $import_form = drupal_get_form('awtw_import_form');
  $output = $import_form;
  return $output;
}

/**
 *  Construct the filter form to filter the list of nodes to export
 */
function awtw_filter_form() {
  $form['source'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Source'), 
    '#tree' => TRUE,
  );
  
  // Filter on source language
  $languages =  i18n_supported_languages();
  $form['source']['language'] = array(
    '#type' => 'select', 
    '#title' => t('Language'), 
    '#default_value' =>  i18n_get_lang(), 
    '#options' => $languages, 
    '#description' => t('The language of the source content.'),
  );
  
  // Filter on content type
  $alltypes = node_get_types('names', NULL, TRUE);
  $types = array();
  foreach($alltypes as $typecode=>$typename) {
    if(variable_get("i18n_node_$typecode", 0)) {
	  $types[$typecode] = $typename;
	}
  }  
  $form['source']['types'] = array(
    '#type' => 'select', 
    '#title' => t('Type'), 
    '#options' => $types, 
    '#description' => t('Select none to view all content types.'),
	'#multiple' => TRUE,
	'#size'=> 5,
  ); 
  
  // Filter on target language
  $languages[none] = "---";
  $form['source']['nottranslatedlanguage'] = array(
    '#type' => 'select', 
    '#title' => t('Not translated to'), 
    '#default_value' =>  "none", 
    '#options' => $languages, 
    '#description' => t('Select only the nodes without a translation in this language.'),
  );
  $form['source']['submit'] = array('#type' => 'submit', '#value' => t('Filter'));
  return $form;
}

/**
 *  Submit filter form
 */
function awtw_filter_form_submit($form_id,$form_values) {
	$_SESSION['awtw_filter_form'] = $form_values;
}

/**
 *  Construct the node selection form that is used to select the nodes to export
 */
function awtw_selectnodes_form() {

  // Set the source language condition
  $language = $_SESSION['awtw_filter_form']['source']['language'];
  if($language == "") $language = i18n_get_lang();

  // Set the content type condition
  $typecondition = "";  
  $types = $_SESSION['awtw_filter_form']['source']['types'];
  if(!count($types)) {
    $alltypes = node_get_types('names', NULL, TRUE);
    $types = array();
    foreach($alltypes as $typecode=>$typename) {
      if(variable_get("i18n_node_$typecode", 0)) {
	    $types[] = $typecode;
	  }
	} 
  }
  if(count($types)) {
    $typecondition = " AND (n.type = '" . implode("' OR n.type = '",$types) . "')";
  }
  
  // Set the target language condition
  $translatedcondition = "";
  $nottranslatedlanguage =  $_SESSION['awtw_filter_form']['source']['nottranslatedlanguage'];
  if($nottranslatedlanguage!="none") {
    $translatedcondition = " AND (i.trid = 0 OR i.trid NOT IN (SELECT trid FROM {i18n_node} WHERE language = '" . $nottranslatedlanguage . "'))";
  }
  
  // Create and execute the query to get all filtered nodes
  $query  = "SELECT n.nid, n.title FROM {node} n INNER JOIN {i18n_node} i ON n.nid = i.nid WHERE i.language = '" . $language ."'". $typecondition . $translatedcondition . " ORDER BY n.title";
  $result = db_query($query);
  
  // Create the array of filtered nodes
  while($node = db_fetch_object($result)){
  	$nid = $node->nid;
	$title = $node->title;
   	$nodes[$nid] = $title;
  }
  
  // Display a summary of the current filter
  $typestring = "all types";
  if(count($types)) $typestring = implode(",",$types);
  $form['filtergroup'] = array(
    '#type' => 'fieldset',
	'#title' => t('Current filter'),
  ); 
  $filterstring = "<strong>Language:</strong> $language<br><strong>Types:</strong> $typestring<br><strong>Not translated to:</strong> $nottranslatedlanguage";
  $form['filtergroup']['filters'] = array(
	'#value' => $filterstring,
  );

  // Display a multiple select list containing all filtered nodes
  $form['export'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Export'), 
    '#tree' => TRUE,
  );  
  if(count($nodes)) {
    $form['export']['selectednodes'] = array(
      '#type' => 'select', 
      '#title' => t('Nodes'), 
      '#options' => $nodes, 
	  '#multiple' => TRUE,
	  '#size'=> 10,
      '#description' => t('Select none to export all nodes.'),
    );  
    $form['export']['submit'] = array('#type' => 'submit', '#value' => t('Export'));
  }
  else {
    $form['export']['noresults'] = array(
	  '#value' => "<strong>There are no nodes that match your filter.</strong>",
	);
  }
  return $form;
}

/**
 *  Submit node selection form
 */
function awtw_selectnodes_form_submit($form_id,$form_values) {
  $selectednodes = $form_values['export']['selectednodes'];
 
  // If there is no node selected, export ALL nodes from that filter 
  if(!count($selectednodes)) {
    $language = $_SESSION['awtw_filter_form']['source']['language'];
    if($language == "") $language = i18n_get_lang();
    $types = $_SESSION['awtw_filter_form']['source']['types'];
    $typecondition = "";
    if(count($types)) $typecondition = " AND (n.type = '" . implode("' OR n.type = '",$types) . "')";
    $query  = "SELECT n.nid, n.title FROM {node} n INNER JOIN {i18n_node} i ON n.nid = i.nid WHERE i.language = '" . $language ."'". $typecondition . " ORDER BY n.title";
    $result = db_query($query);
  
    while($node = db_fetch_object($result)){
  	  $nid = $node->nid;
   	  $selectednodes[] = $nid;
	}
  }
  
  // Get the path where we will create the export file: export#userid#.xml
  $exportpath = variable_get('file_directory_path', 'files') . "/awtwexport/";
  if(!file_exists($exportpath)) {
  	mkdir($exportpath);
  }
  global $user;
  $myFile = $exportpath . "export".$user->uid.".xml";
  $fh = fopen($myFile, 'w');

  // Start XML output
  $stringData = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"
  			  . "\t<export>\n"
			  . "\t\t<elements>\n";
			  
  // Loop through all selected nodes and create XML element for each one, containing all data
  foreach($selectednodes as $node_id) {
    $node = node_load($node_id);
	
	// Check all CCK fields of this type
	$cck_string = "";
	if(module_exists('cck')) {
		$cck_fields_tmp = content_types($node->type);
		$cck_fields_tmp = $cck_fields_tmp['fields'];
		foreach($cck_fields_tmp as $fieldname=>$fieldarray) {
		  if($fieldarray['type'] == "text" && $fieldarray['widget']['type'] == "text") {
			$fieldvalues = array();
			foreach($node->$fieldname as $tmpvalue) {
			  $fieldvalues[] = $tmpvalue['value'];
			}
			$cck_string .= "\t\t\t\t<$fieldname><![CDATA[<p>".implode("[AND]",$fieldvalues)."</p>]]></$fieldname>\n";
		  }
		}
	}	
	
    $stringData .= "\t\t\t<item>\n"
			     . "\t\t\t\t<nid>$node->nid</nid>\n"
				 . "\t\t\t\t<title><![CDATA[<p>$node->title</p>]]></title>\n"
				 . "\t\t\t\t<body><![CDATA[<p>$node->body</p>]]></body>\n"
				 . "$cck_string"
				 . "\t\t\t</item>\n";
  }
  
  // End XML output
  $stringData .= "\t\t</elements>\n"
  			   . "\t</export>";
			   
  // Write XML output to the export file and close the file
  fwrite($fh, $stringData);
  fclose($fh);
  
  // Display link to the export file as a system message
  global $base_url;
  $link = $base_url . "/" . variable_get('file_directory_path', 'files') . '/awtwexport/export'.$user->uid.'.xml';
  drupal_set_message('Click <a href="'.$link.'" target="_blank">here</a> to download your export.');
}

/**
 *  Construct the import form
 */
function awtw_import_form() {

  // Set the target language condition
  $languages =  i18n_supported_languages();
  $form['language'] = array(
    '#type' => 'select', 
    '#title' => t('Language'), 
    '#default_value' =>  i18n_get_lang(), 
    '#options' => $languages, 
    '#description' => t('The language of the translated content.'),
  );
  
  // Upload the translated XML export
  $form['xml_upload'] = array(
  '#type' => 'file', 
  '#title' => t('Translated XML'), 
  '#size' => 40,
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  
  // Set form attributes to multipart/form-data to allow file upload
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  
  return $form;
}

/**
 *  Submit import form
 */
$awtw_xmlarray = array();
$awtw_xmlobject = array();
$awtw_include = false;
$awtw_xmlname = "";
 
function awtw_import_form_submit($form_id,$form_values) {
  global $user;
  global $awtw_xmlarray;
  global $awtw_xmlobject;
  $translated_language = $form_values['language'];

  // Get the path where we will upload the imported file
  $importpath = variable_get('file_directory_path', 'files') . "/awtwimport/";  
  if(!file_exists($importpath)) {
  	mkdir($importpath);
  }
  
  // Upload file to server  
  if ($file = file_check_upload('xml_upload')) {
    $file = file_save_upload('xml_upload',"awtwimport/".$user->uid.$file->filename, 1);
  }
  else {
    drupal_set_message("Error when uploading the file.",'error');
  }
  
  // Read and parse XML
  $xmlfile = fopen($file->filepath,'r');
  $xmlcontent = '';
  while (!feof($xmlfile) && $chunk = fread($xmlfile, 1024)) {
    $xmlcontent .= $chunk;
  }
  $xml_parser = xml_parser_create(); 
  xml_set_element_handler($xml_parser,"awtw_start_tag","awtw_end_tag");
  xml_set_character_data_handler($xml_parser, "awtw_contents_tag");
  if(!(xml_parse($xml_parser, $xmlcontent, TRUE))){ 
    drupal_set_message("Error on line " . xml_get_current_line_number($xml_parser),'error'); 
  } 
  
  // Initialize counters
  $new_nodes = 0;
  $updated_nodes = 0;
  
  // Loop through items  
  foreach($awtw_xmlarray as $translated_node) {
  	// Implode all arrays to strings
  	foreach($translated_node as $key => $value) { 
	  $translated_node[$key] = implode("",$translated_node[$key]);
	  // Replace all '&apos;' by ' because this is always exported by DEJA VU
	  $translated_node[$key] = str_replace("&apos;","'",$translated_node[$key]);
	  // Substr to cut off the <p> at the beginning and </p> at the end that was added for DEJA VU
	  if($key != 'nid') {
	    $translated_node[$key] = substr($translated_node[$key],3,strlen($translated_node[$key])-7);
	  }
	  // For all custom text fields, split the string to an array
	  if(strpos($key,"ield_")==1) {
	  	$translated_node[$key] = explode("[AND]",$translated_node[$key]);
		foreach($translated_node[$key] as $subkey => $subvalue) {
		  $translated_node[$key][$subkey] = array("value" => $subvalue);
		}
	  }
	}
  	$original_nid = $translated_node['nid'];
	$original_language = i18n_node_get_lang($original_nid);
	
	// Check whether a translation in this language already exists
	$translations = translation_node_get_translations(array('nid'=>$original_nid), TRUE);
	// If the translation exists, take this node as original
	if( $translations[$translated_language]->nid != NULL) {
	  $updated_nodes++;
	  $translated_node['nid'] = $translations[$translated_language]->nid;
 	  $original_node = node_load($translations[$translated_language]->nid);
	}
	// Else create a new node with the source language original
	else {
	  $new_nodes++;
	  $translated_node['nid'] = NULL;
	  $original_node = node_load($original_nid);
	}
	
	// Set all values into the translated_node array from the previous translation or from the original node
	$fields = array("type","status","comment","format","promote","sticky","trid");
	foreach($fields as $field) {
	  $translated_node[$field] = $original_node->$field;
	}
	$translated_node['revision'] = TRUE;
	$translated_node['language'] = $translated_language;
	$translated_node['i18n_status'] = 3;

	// Set all CCK fields that were not translated (images, numbers, select lists, etc) as the original node
	// Check all CCK fields of this type
	if(module_exists('cck')) {
		$cck_fields_tmp = content_types($original_node->type);
		$cck_fields_tmp = $cck_fields_tmp['fields'];
		foreach($cck_fields_tmp as $fieldname=>$fieldarray) {
		  if(!($fieldarray['type'] == "text" && $fieldarray['widget']['type'] == "text")) {
			$translated_node[$fieldname] = $original_node->$fieldname;
		  }
		}
	}
	
	// Check whether the node is a book page, if yes, link to translation of parent
	$source_node = node_load($original_nid);
	if($original_parent = $source_node->parent) {
	  drupal_set_message("book page");
	  $parent_translations = translation_node_get_translations(array('nid'=>$original_parent), TRUE);
	  if($translated_parent = $parent_translations[$translated_language]->nid) {
	    $translated_node['parent'] = $translated_parent;
	  }
	}	
	
    // Translate taxonomy terms (from translation.module)
	// To make this work with pathauto, the following adaptation to pathauto_node.inc is necessary:
	// line 214: if($first_term_id->tid != NULL) $first_term_id = $first_term_id->tid;
    if(isset($source_node->taxonomy) && is_array($source_node->taxonomy)) {
      // Set translated taxonomy terms
   	  $taxonomy = array();
      foreach ($source_node->taxonomy as $tid => $term) {
        if ($term->language) {
          $translated_terms = translation_term_get_translations(array('tid' =>$tid));
          if($translated_terms && $newterm = $translated_terms[$translated_language]) {
            $taxonomy[$newterm->tid] = $newterm;
          }
        } else {
          // Term has no language. Should be ok
          $taxonomy[$tid] = $term;
        }
      }
      $translated_node['taxonomy'] = $taxonomy;
	}
	

	// Create new node or revision of node based on the array $translated_node
	$new_nid = substr(node_form_submit('awtw_import', $translated_node),5);	
	// If the translation didn't exist already, link the translated node that was created to the original one.
	if($translated_node['nid']==NULL) {
	  $translation['source_nid'] = $original_nid;
	  $translation['language'] = $translated_language;
	  $translation['nid'] = $new_nid;
	  if($translated_node['trid']!=0) $translation['trid'] = $translated_node['trid'];
	  translation_node_form_submit('awtw_import',$translation);
	 }
  }
  
  $_SESSION['messages']['status'] = array();
  drupal_set_message(t('New nodes created: %new_nodes',array('%new_nodes' => $new_nodes)));
  drupal_set_message(t('Existing nodes updated: %updated_nodes',array('%updated_nodes' => $updated_nodes)));
  
}

/**
 *  XML Parse function: start of the tag
 */
function awtw_start_tag($parser, $name, $attrs) {
    global $awtw_xmlarray;
	global $awtw_xmlobject;
	global $awtw_include;
	global $awtw_xmlname;

	// Only include the XML tags with actual content
	if($name=="NID" || $name=="BODY" || $name=="TITLE" || strpos($name,"IELD_")==1) {
	  $awtw_include = true;
	  $awtw_xmlname = strtolower($name);
	}
}

/**
 *  XML Parse function: end of the tag
 */
function awtw_end_tag($parser, $name) {
    global $awtw_xmlarray;
	global $awtw_xmlobject;
	global $awtw_include;
	global $awtw_xmlname;

	// When the item is finished, add the current object-array to the full array and create a new object-array
    if($name=='ITEM') {
	  $awtw_xmlarray[] = $awtw_xmlobject;
      $awtw_xmlobject = array();
	}
	$awtw_include = false;
	$awtw_xmlname = "";	
}

/**
 *  XML Parse function: content of the tag
 */
function awtw_contents_tag($parser, $data) {
	global $awtw_xmlobject;
	global $awtw_include;
	global $awtw_xmlname;

	// Only include the XML tags with actual content	
	if($awtw_include) {
		$awtw_xmlobject[$awtw_xmlname][] = $data;
	}
}

?>